package com.android.learn.advanced.android.assignment

import com.android.learn.advanced.android.base.BaseResponseModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class AssignmentResponseModel (

    @SerializedName("gid")
    val id : String,

    @SerializedName("name")
    val name : String,

    @SerializedName("start_date")
    val startDate : Date,

    @SerializedName("end_date")
    val endDate : Date

) : BaseResponseModel()