package com.android.learn.advanced.android.assignment

import android.view.View
import com.android.learn.advanced.android.base.BaseViewHolder
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.row_assignment.view.*

class AssignmentViewHolder (itemView: View) : BaseViewHolder<AssignmentViewModel, AssignmentRowActionModel>(itemView) {

    override fun bind(vm: AssignmentViewModel) {

        itemView.txt_storeName.text = vm.name
    }

    override fun onItemClick(
        vm: AssignmentViewModel,
        actionSubject: PublishSubject<AssignmentRowActionModel>
    ) {
        itemView.rootView.clicks()
            .map {
                AssignmentRowActionModel.ItemClick(vm)
            }
            .subscribe(actionSubject)
    }
}