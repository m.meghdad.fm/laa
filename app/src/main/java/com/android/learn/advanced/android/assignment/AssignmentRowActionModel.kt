package com.android.learn.advanced.android.assignment

import com.android.learn.advanced.android.base.BaseViewHolderAction

sealed class AssignmentRowActionModel (val selectedItem : AssignmentViewModel) : BaseViewHolderAction<AssignmentViewModel>(selectedItem){

    class ItemClick (selectedItem : AssignmentViewModel) : AssignmentRowActionModel(selectedItem)
//    class ItemLongClick(selectedItem : AssignmentViewModel) : AssignmentRowActionModel(selectedItem)
//    class FavoritetnClick(selectedItem : AssignmentViewModel) : AssignmentRowActionModel(selectedItem)
}
