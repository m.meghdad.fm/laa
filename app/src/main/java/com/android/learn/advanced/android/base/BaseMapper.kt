package com.android.learn.advanced.android.base

abstract class BaseMapper <RM : BaseResponseModel, VM : BaseViewModel> {

    abstract fun map(responseItem : RM) : VM?

    fun listMap(responseItems: List<RM>?): List<VM>? {

        if (responseItems.isNullOrEmpty())
            return null

        val viewItems: MutableList<VM> = mutableListOf()

        responseItems.forEach { rm ->
            map(rm)?.let { vm ->
                viewItems.add(vm)
            }
        }

        return viewItems.toList()
    }

}