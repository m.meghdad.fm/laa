package com.android.learn.advanced.android.assignment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.learn.advanced.android.R
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class AssignmentAdapter : RecyclerView.Adapter<AssignmentViewHolder>() {

    private var viewItems: List<AssignmentViewModel>? = null
    private val itemClickSubject = PublishSubject.create<AssignmentRowActionModel>()


    fun setData(viewItems: List<AssignmentViewModel>?) {

        this.viewItems = viewItems
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssignmentViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_assignment, parent, false)
        return AssignmentViewHolder(view)
    }

    override fun onBindViewHolder(holder: AssignmentViewHolder, position: Int) {

        val assignment = viewItems?.get(position) ?: AssignmentViewModel()

        holder.bind(assignment)
        holder.onItemClick(assignment, itemClickSubject)

    }

    override fun getItemCount(): Int = viewItems?.size ?: 0


    fun getItemClicks() : Observable<AssignmentRowActionModel> {

        return itemClickSubject
            .hide()
    }
}

