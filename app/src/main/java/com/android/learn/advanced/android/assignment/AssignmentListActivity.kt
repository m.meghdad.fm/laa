package com.android.learn.advanced.android.assignment

import android.os.Bundle
import android.os.PersistableBundle
import com.android.learn.advanced.android.databinding.ActivityAssignmentListBinding
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.nio.file.attribute.AttributeView

class AssignmentListActivity :
    MviActivity<AssignmentListView, AssignmentListPresenter>(),
        AssignmentListView
{

    lateinit var binding : ActivityAssignmentListBinding

    private val firstLoadSubject : PublishSubject<Unit> = PublishSubject.create()

    private var adapter = AssignmentAdapter()


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        binding = ActivityAssignmentListBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.recyclerview.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        firstLoadSubject.onNext(Unit)
    }


    override fun createPresenter(): AssignmentListPresenter = AssignmentListPresenter()

    // Emits
    override fun emitFirstLoad(): Observable<Unit> = firstLoadSubject
    override fun emitRefreshList(): Observable<Unit> = binding.listRefreshSwiper.refreshes()
    override fun emitItemClick(): Observable<AssignmentRowActionModel> = adapter.getItemClicks()

    // Display
    override fun display(vs: AssignmentListViewState) {

        binding.listRefreshSwiper.isRefreshing = false

        when(vs.state) {
            AssignmentListActionState.FirstLoadState -> renderFirstLoad()
            AssignmentListActionState.ListLoadState -> renderListLoad()
            AssignmentListActionState.StaleState -> renderStale()
            is AssignmentListActionState.ListDataState -> renderListData(vs.viewItems)
            is AssignmentListActionState.ListErrorState -> renderListError(vs.loadListError)
            is AssignmentListActionState.GoAssignmentDetailState -> goAssignmentDetailActivity(vs.selectedItem)
        }
    }

    private fun renderFirstLoad() {}
    private fun renderStale() {}

    private fun renderListLoad() {

        binding.listRefreshSwiper.isRefreshing = true
    }

    private fun renderListData(viewItems: List<AssignmentViewModel>?) {

        adapter.setData(viewItems)
    }

    private fun renderListError(loadListError: Throwable?) {}
    private fun goAssignmentDetailActivity(selectedItem: AssignmentResponseModel?) {

    }
}