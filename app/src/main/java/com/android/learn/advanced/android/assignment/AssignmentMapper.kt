package com.android.learn.advanced.android.assignment

import com.android.learn.advanced.android.base.BaseMapper

class AssignmentMapper : BaseMapper<AssignmentResponseModel, AssignmentViewModel>() {

    override fun map(responseItem: AssignmentResponseModel): AssignmentViewModel? {

        return AssignmentViewModel(
            id = responseItem.id,
            name = responseItem.name
        )
    }

}
