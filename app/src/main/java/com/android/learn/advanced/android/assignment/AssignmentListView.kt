package com.android.learn.advanced.android.assignment

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface AssignmentListView : MvpView{

    // Actions
    fun emitFirstLoad() : Observable<Unit>
    fun emitRefreshList() : Observable<Unit>
    fun emitItemClick() : Observable<AssignmentRowActionModel>

    // Display
    fun display(vs : AssignmentListViewState)
}