package com.android.learn.advanced.android.assignment

import com.android.learn.advanced.android.base.BaseViewModel

data class AssignmentViewModel (

    val id : String? = null,
    val name : String? = null

) : BaseViewModel()
