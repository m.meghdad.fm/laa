package com.android.learn.advanced.android.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.PublishSubject

abstract class BaseViewHolder<VM, VHA : BaseViewHolderAction<VM>>(itemView : View) :
    RecyclerView.ViewHolder(itemView) {

    abstract fun bind(vm : VM)
    abstract fun onItemClick(vm : VM, actionSubject: PublishSubject<VHA>)
}