package com.android.learn.advanced.android.assignment

sealed class AssignmentListActionState {

    object FirstLoadState : AssignmentListActionState()

    object ListLoadState : AssignmentListActionState()
    object StaleState : AssignmentListActionState()

    data class ListDataState(val viewItems : List<AssignmentViewModel>? = null) : AssignmentListActionState()
    data class ListErrorState(val error : Throwable? = null) : AssignmentListActionState()

    data class GoAssignmentDetailState(val selectedItem : AssignmentResponseModel? =  null) : AssignmentListActionState()
}
