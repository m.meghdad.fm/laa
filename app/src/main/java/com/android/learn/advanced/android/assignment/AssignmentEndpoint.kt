package com.android.learn.advanced.android.assignment

import io.reactivex.Observable
import retrofit2.http.GET

interface AssignmentEndpoint {

    @GET("/api/assignment")
    fun getAssignments() : Observable<List<AssignmentResponseModel>>
}