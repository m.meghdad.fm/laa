package com.android.learn.advanced.android.assignment

data class AssignmentListViewState (

    val state : AssignmentListActionState,

    val viewItems : List<AssignmentViewModel>? = null,
    val loadListError : Throwable? = null,
    val selectedItem : AssignmentResponseModel? =  null
)


