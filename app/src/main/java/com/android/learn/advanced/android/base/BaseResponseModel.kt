package com.android.learn.advanced.android.base

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseResponseModel : Parcelable
