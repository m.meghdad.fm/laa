package com.android.learn.advanced.android

import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


object HttpManager {

    const val mainUrl: String = "host"


    fun getRetrofitObject(): Retrofit {
        return createRetrofit(mainUrl, createHttpClient())
    }

    private fun createRetrofit(baseUrl: String, client: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createHttpClient(): OkHttpClient {

        val builder = OkHttpClient.Builder()

        builder

//            .connectTimeout(APIConfig.ConnectTimeOut, TimeUnit.SECONDS)// Set connection timeout
//            .readTimeout(APIConfig.ReadTimeout, TimeUnit.SECONDS)// Set Read timeout
//            .writeTimeout(APIConfig.WriteTimeout, TimeUnit.SECONDS)// Set Write timeout
//            .addInterceptor(CommonHeaderInterceptor()) // User-Agent
            .addInterceptor(provideHttpLoggerInterceptor())  // To display the request and response details in log
//            .addInterceptor(AuthInterceptor()) // add token

        return builder.build()
    }

    private fun provideHttpLoggerInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

}