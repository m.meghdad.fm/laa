package com.android.learn.advanced.android.assignment

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class AssignmentListPresenter : MviBasePresenter<AssignmentListView, AssignmentListViewState>() {

    private val useCase = AssignmentListUseCase()

    override fun bindIntents() {

        val firstLoad: Observable<AssignmentListActionState> =
            intent(AssignmentListView::emitFirstLoad)
                .subscribeOn(Schedulers.io())
                .debounce(20, TimeUnit.MILLISECONDS)
                .flatMap {
                    useCase.firstLoad()
                }
                .observeOn(AndroidSchedulers.mainThread())

        val refreshList: Observable<AssignmentListActionState> =
            intent(AssignmentListView::emitRefreshList)
                .subscribeOn(Schedulers.io())
                .debounce(300, TimeUnit.MILLISECONDS)
                .flatMap {
                    useCase.getDataByEndpoint()
                }
                .observeOn(AndroidSchedulers.mainThread())

        val itemClick: Observable<AssignmentListActionState> =
            intent(AssignmentListView::emitItemClick)
                .subscribeOn(Schedulers.io())
                .debounce(300, TimeUnit.MILLISECONDS)
                .flatMap {
                    useCase.itemClick(it)
                }
                .observeOn(AndroidSchedulers.mainThread())

        val allActionStates = Observable.merge(
            firstLoad,
            refreshList,
            itemClick
        )

        val initialViewState = AssignmentListViewState(state = AssignmentListActionState.FirstLoadState)

        val allViewStates: Observable<AssignmentListViewState> = allActionStates
            .scan(initialViewState, this::stateReducer)

        // Subscribe to View States --- listen to allViewStates -> then ->  do display
        subscribeViewState(allViewStates, AssignmentListView::display)
    }

    private fun stateReducer(
        previousState: AssignmentListViewState,
        currentState: AssignmentListActionState
    ): AssignmentListViewState {

        return when (currentState) {
            AssignmentListActionState.FirstLoadState,
            AssignmentListActionState.ListLoadState,
            AssignmentListActionState.StaleState
            -> {
                previousState.copy(
                    state = currentState
                )
            }
            is AssignmentListActionState.ListDataState -> {
                previousState.copy(
                    state = currentState,
                    viewItems = currentState.viewItems
                )
            }
            is AssignmentListActionState.ListErrorState -> {
                previousState.copy(
                    state = currentState,
                    loadListError = currentState.error
                )
            }
            is AssignmentListActionState.GoAssignmentDetailState -> {
                previousState.copy(
                    state = currentState,
                    selectedItem = currentState.selectedItem
                )
            }
        }
    }
}