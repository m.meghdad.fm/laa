package com.android.learn.advanced.android.assignment

import com.android.learn.advanced.android.HttpManager
import io.reactivex.Observable

class AssignmentListUseCase {

    private val endpoint: AssignmentEndpoint = HttpManager.getRetrofitObject().create(AssignmentEndpoint::class.java)
    private val mapper = AssignmentMapper()

    private var responseItems : List<AssignmentResponseModel>? = null
    private var viewItems : List<AssignmentViewModel>? = null


    fun firstLoad(): Observable<AssignmentListActionState> {

        return getDataByEndpoint()
    }

    fun getDataByEndpoint(): Observable<AssignmentListActionState> {

        return endpoint.getAssignments()
            .map<AssignmentListActionState> {
                responseItems = it
                viewItems = mapper.listMap(responseItems)
                AssignmentListActionState.ListDataState(viewItems)
            }
            .startWith(AssignmentListActionState.ListLoadState)
            .onErrorReturn{
                AssignmentListActionState.ListErrorState(it)
            }
    }

    fun itemClick(action: AssignmentRowActionModel): Observable<AssignmentListActionState> {

        val selectedItemID = action.selectedItem.id
        val selectedItemRM = responseItems?.find { item -> item.id == selectedItemID }

        return Observable.just(Unit)
            .map <AssignmentListActionState>{
                when(action){
                    is AssignmentRowActionModel.ItemClick -> {
                        AssignmentListActionState.GoAssignmentDetailState(selectedItemRM)
                    }
                }
            }
    }


}
